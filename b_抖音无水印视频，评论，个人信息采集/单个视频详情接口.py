import json

import requests

# 请求参数为视频id
'''
请求参数：aweme_id为视频的id
请求响应:json字符串,需要使用json.loads()变为dict
'''
data = {"aweme_id": "7055063486392487206"}
res = requests.post('http://49.235.82.99:9988/video_details', data=data)
res = json.loads(res.text)

print('视频标题', res['aweme_detail']['desc'])
print('收藏数,评论数,点赞数,分享数', res['aweme_detail']['statistics'])
print('视频地址', res['aweme_detail']['video']['play_addr']['url_list'][0])




