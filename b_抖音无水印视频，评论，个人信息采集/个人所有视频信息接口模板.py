import json
import time
import requests

'''
说明:
1.每次请求响应最多十条作品信息
2.请求参数说明:{
        'max_cursor'(第一次为0,后面的均为上次响应返回中包含的max_cursor的值),
        'secUid': (用户的安全id)
        }
        
3.请求响应说明:{
        '返回类型为':json字符串,使用json.loads()即可转换为dict
        '返回内容':最多包含作品信息,以及max_cursor(下一次请求使用)ps若没有作品信息,max_cursor的值为'null',
        }


--------------------------
例子:
# 用户有9条作品，需要请求2次，
第一次请求:max_cursor:0(第一次请求均设为0)
第一次响应:9条作品的信息，以及下一次请求max_cursor参数的值

第二次请求:上一次请求的max_cursor的值
第二次响应:0作品，以及max_cursor的值为'null'，为null说明所有数据采集完成
--------------------------------
# 用户有19条作品，需要请求3次，
第一次请求:max_cursor:0
第一次响应:10条作品的信息，以及下一次请求max_cursor参数的值

第二次请求:上一次请求的max_cursor的值
第二次响应:剩下9条作品的值,下一次请求max_cursor参数的值

第三次请求:上一次请求的max_cursor的值
第三次响应:0作品，以及max_cursor的值为'null'，为null说明所有数据采集完成
'''

data = {
    'max_cursor': 0,
    'secUid': 'MS4wLjABAAAAGMJ1guYSdOb1376WfTZ2aDU8rfS46svBh0oyhKossiI',
}
a = 1
all_video_url = 'http://49.235.82.99:9988//person_all_videos'
while True:
    res = requests.post(all_video_url, data=data)
    res = json.loads(res.text)
    max_cursor = res['max_cursor']
    data['max_cursor'] = max_cursor
    if max_cursor == 'null':
        break
    for i in res['aweme_list']:
        print('视频标题', i['desc'])
        print('音乐地址', i['music']['play_url']['uri'])
        # 自行解析
        print('收藏数,评论数,点赞数,分享数', i['statistics'])
        print('视频地址', i['video']['play_addr']['url_list'][0])
        a = a + 1
    time.sleep(2)
