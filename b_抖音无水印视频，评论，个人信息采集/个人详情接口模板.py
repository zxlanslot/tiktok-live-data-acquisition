import json

import requests

# 请求参数为视频id
'''
请求参数{'secUid':用户安全id
       }
请求响应:json字符串,需要使用json.loads()变为dict
'''
data = {
    "secUid": "MS4wLjABAAAA3loyxnyCcj-fwGP0jVMEJNhy1n5z1wE9LvWN28_PuIw"

    }

res = requests.post('http://49.235.82.99:9988/person_details', data=data)
res = json.loads(res.text)
# 抖音号，我发现不同长度的安全id，抖音号存在不同位置。
short_id = res['user']['short_id']
user_id = res['user']['unique_id']
if short_id != '0' and len(user_id) == 0:
    user_id = short_id
print('抖音号:', user_id)
nickname = res['user']['nickname']
print('昵称:', nickname)

uid = res['user']['uid']
print('uid:', uid)

sec_uid = res['user']['sec_uid']
print('sec_uid:', sec_uid)


if 'ip_location' in res['user']:
    ip_location = res['user']['ip_location']
    print('ip属地:', ip_location)

if 'country' in res['user']:
    country = res['user']['country']
    if country != '':
        print('国家', country)


if 'city' in res['user']:
    city = res['user']['city']
    if city != '':
        print('城市', city)
# province

if 'province' in res['user']:
    province = res['user']['province']
    if province != '':
        print('省份', province)
if 'user_age' in res['user']:
    user_age = res['user']['user_age']
    if user_age>0:
        print('年龄',user_age)
signature = res['user']['signature']
print('签名:', signature)

head_img = res['user']['avatar_thumb']['url_list'][0]
print('个人头像地址:', head_img)

share_id = res['user']['share_info']['share_qrcode_url']['url_list'][0]
print('个人二维码地址:', share_id)






