import json
import time
import requests
'''
请求参数{'aweme_id':视频id,
        'cursor':第一次请求为0，下面的请求要一次加20
        }
请求响应:json字符串,需要使用json.loads()变为dict
----------------
采集的评论均为一级评论（不含回复此评论的评论）
每次请求中最多包含20条评论
当请求中响应终端comments列表为空时，采集结束。

'''
data = {"aweme_id": '7055063486392487206',
        'cursor': 0
        }

while True:
    res = requests.post('http://49.235.82.99:9988/video_comment', data=data)
    res = json.loads(res.text)
    if res['state'] == 'success':
        for i in res['comments']:
            print(i)
        #     当没有评论时，说明采集完成
        if len(res['comments']) == 0:
            break
    time.sleep(2)
    data['cursor'] += 20
