
## 案例演示-均有源码2022-11-3均有效


### ①某音学习源码案例演示

#### vx：crazy2001010

1. #### rpc版本打印到终端演示
    **主要用途：可以进行二次开发** 
    **rpc缺点：不方便打包** 

  ![输入图片说明](img/rpc_%E6%89%93%E5%8D%B0%E5%88%B0%E7%BB%88%E7%AB%AF.png)

1. #### dy协议版本打印到终端演示
    **主要用途：可以进行二次开发** 
    **优点：方便打包成exe** 
![输入图片说明](img/dy%E5%8D%8F%E8%AE%AE%E6%89%93%E5%8D%B0%E5%88%B0%E7%BB%88%E7%AB%AF.png)




1. #### dy弹幕游戏接口版本
     **主要用途：主要用配合unity或者虚幻引擎（UE5）弹幕游戏使用，传输数据为JSON字符串** 
![输入图片说明](img/dy%E5%BC%B9%E5%B9%95%E6%B8%B8%E6%88%8F%E7%89%88%E6%9C%AC.png)

1. #### dy其他接口
**视频评论/个人信息/视频信息/关键词搜索/直播视频flv地址** 

![输入图片说明](img/%E4%B8%AA%E4%BA%BA%E8%AF%A6%E6%83%85.png)

**评论采集** 

![输入图片说明](img/%E8%AF%84%E8%AE%BA%E9%87%87%E9%9B%86.png)


    




### ①kuaishou案例演示


1. #### 普通http版本打印到终端演示
 **主要用途:可以进行二次开发，只含有用户发言** 

 ![输入图片说明](img/ks%E5%8F%91%E8%A8%80.png)


2. #### 弹幕游戏接口版本
  **主要用途：可以进行二次开发，信息齐全（用户刷礼物，用户发言，用户点赞）** 

 ![输入图片说明](img/ks%E6%89%93%E5%8D%B0%E5%88%B0%E4%B8%AD%E7%AB%AF.png)


3. #### 弹幕游戏接口版本
 **主要用途：主要用配合unity或者虚幻引擎（UE5）弹幕游戏使用，传输数据为JSON字符串** 
![输入图片说明](img/ks%E6%B8%B8%E6%88%8F%E5%BC%B9%E5%B9%95%E6%8E%A5%E5%8F%A3.png)


4. #### 直播间采集版本

 **主要用途：信息采集** 

![输入图片说明](img/ks%E4%BA%8C%E7%BB%B4%E7%A0%81%E9%87%87%E9%9B%86.png)







#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
